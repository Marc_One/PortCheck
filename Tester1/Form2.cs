﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Win32;

namespace Tester1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            cbOnSave.Checked = Config.OptOnSave > 0;
            numColSwitch.Value = Config.OptColSwitch;
            numRowFirst.Value = Config.OptRowFirst;
            numColPort.Value = Config.OptColPort;
            numMinSwCount.Value = Config.OptMinSwCount;
            numColDose.Value = Config.OptColDose;

        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            Config.OptOnSave = Convert.ToInt32(cbOnSave.Checked);
            Config.OptColSwitch = Convert.ToInt32(numColSwitch.Value);
            Config.OptRowFirst = Convert.ToInt32(numRowFirst.Value);
            Config.OptColPort = Convert.ToInt32(numColPort.Value);
            Config.OptMinSwCount = Convert.ToInt32(numMinSwCount.Value);
            Config.OptColDose = Convert.ToInt32(numColDose.Value);

            RegistryKey MyKey = Registry.CurrentUser.CreateSubKey("Software\\" + Config.REG_KEY);

            MyKey.SetValue("OnSave", Config.OptOnSave);
            MyKey.SetValue("ColSwitch", Config.OptColSwitch);
            MyKey.SetValue("RowFirst", Config.OptRowFirst);
            MyKey.SetValue("ColPort", Config.OptColPort);
            MyKey.SetValue("MinSwCount", Config.OptMinSwCount);
            MyKey.SetValue("ColDose", Config.OptColDose);

            this.Hide();
        }

        private void bStandard_Click(object sender, EventArgs e)
        {
            cbOnSave.Checked =  false;
            numColSwitch.Value = Config.StdColSwitch;
            numRowFirst.Value = Config.StdRowFirst;
            numColPort.Value = Config.StdColPort;
            numMinSwCount.Value = Config.StdMinSwCount;
            numColDose.Value = Config.StdColDose;
        }
    }
}
