﻿namespace Tester1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbOnSave = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numColSwitch = new System.Windows.Forms.NumericUpDown();
            this.numRowFirst = new System.Windows.Forms.NumericUpDown();
            this.numColPort = new System.Windows.Forms.NumericUpDown();
            this.numMinSwCount = new System.Windows.Forms.NumericUpDown();
            this.numColDose = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.bSave = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.bStandard = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numColSwitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRowFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinSwCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColDose)).BeginInit();
            this.SuspendLayout();
            // 
            // cbOnSave
            // 
            this.cbOnSave.AutoSize = true;
            this.cbOnSave.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbOnSave.Location = new System.Drawing.Point(222, 12);
            this.cbOnSave.Name = "cbOnSave";
            this.cbOnSave.Size = new System.Drawing.Size(15, 14);
            this.cbOnSave.TabIndex = 0;
            this.cbOnSave.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Switchname in Spalte (Numerisch)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Erste Zeile nach dem Kopf";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Portname in Spalte(Numerisch)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mindestanzahl von Switcheinträgen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(202, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Dosenbezeichnung in Spalte (Numerisch)";
            // 
            // numColSwitch
            // 
            this.numColSwitch.Location = new System.Drawing.Point(222, 38);
            this.numColSwitch.Name = "numColSwitch";
            this.numColSwitch.Size = new System.Drawing.Size(85, 20);
            this.numColSwitch.TabIndex = 11;
            // 
            // numRowFirst
            // 
            this.numRowFirst.Location = new System.Drawing.Point(222, 67);
            this.numRowFirst.Name = "numRowFirst";
            this.numRowFirst.Size = new System.Drawing.Size(85, 20);
            this.numRowFirst.TabIndex = 12;
            // 
            // numColPort
            // 
            this.numColPort.Location = new System.Drawing.Point(222, 94);
            this.numColPort.Name = "numColPort";
            this.numColPort.Size = new System.Drawing.Size(85, 20);
            this.numColPort.TabIndex = 13;
            // 
            // numMinSwCount
            // 
            this.numMinSwCount.Location = new System.Drawing.Point(222, 121);
            this.numMinSwCount.Name = "numMinSwCount";
            this.numMinSwCount.Size = new System.Drawing.Size(85, 20);
            this.numMinSwCount.TabIndex = 14;
            // 
            // numColDose
            // 
            this.numColDose.Location = new System.Drawing.Point(222, 148);
            this.numColDose.Name = "numColDose";
            this.numColDose.Size = new System.Drawing.Size(85, 20);
            this.numColDose.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Automatische Prüfung beim Speichern";
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(12, 204);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 17;
            this.bSave.Text = "Speichern";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(232, 204);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 18;
            this.bCancel.Text = "Abbrechen";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bStandard
            // 
            this.bStandard.Location = new System.Drawing.Point(122, 204);
            this.bStandard.Name = "bStandard";
            this.bStandard.Size = new System.Drawing.Size(75, 23);
            this.bStandard.TabIndex = 19;
            this.bStandard.Text = "Standard";
            this.bStandard.UseVisualStyleBackColor = true;
            this.bStandard.Click += new System.EventHandler(this.bStandard_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 239);
            this.Controls.Add(this.bStandard);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numColDose);
            this.Controls.Add(this.numMinSwCount);
            this.Controls.Add(this.numColPort);
            this.Controls.Add(this.numRowFirst);
            this.Controls.Add(this.numColSwitch);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbOnSave);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.numColSwitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRowFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinSwCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColDose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbOnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numColSwitch;
        private System.Windows.Forms.NumericUpDown numRowFirst;
        private System.Windows.Forms.NumericUpDown numColPort;
        private System.Windows.Forms.NumericUpDown numMinSwCount;
        private System.Windows.Forms.NumericUpDown numColDose;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bStandard;
    }
}