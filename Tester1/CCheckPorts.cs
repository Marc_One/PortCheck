﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Tester = Microsoft.Office.Tools.Excel;

using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Tester1
{   
    class CCheckPorts
    {
        Excel.Application MyApp;
        Excel.Workbook MyWB;

        public CCheckPorts()
        {
            MyApp = Globals.ThisAddIn.Application;
            MyWB = MyApp.ActiveWorkbook;
        }

        private void FillList(CRaum Raum)
        {
            foreach (Excel.Worksheet MySheet in MyWB.Worksheets)
            {
                int numRows = MySheet.UsedRange.Rows.Count;
                if(numRows >= 1000)
                {
                    System.Windows.Forms.MessageBox.Show("Mehr als 1000 Zeilen in Datei entdeckt!! Breche ab!");
                    return;
                }
                for (int i = Config.OptRowFirst; i <= numRows; i++)
                {
                    string LSwitch = Convert.ToString(MySheet.Cells[i, Config.OptColSwitch].Value ?? "").ToUpper();
                    string LPort = Convert.ToString(MySheet.Cells[i, Config.OptColPort].Value ?? "").ToUpper();
                    string LDose = Convert.ToString(MySheet.Cells[i, Config.OptColDose].Value ?? "").ToUpper();
                    string LBemerkung = Convert.ToString(MySheet.Cells[i, Config.OptColBem].Value ?? "").ToUpper();
                    string LRack = MySheet.Name.ToUpper();
                    if (Utils.IsPatchOK(LSwitch, LPort))
                        Raum.Add(LSwitch, LPort, LRack, LDose, LBemerkung);
                }
            }
        }

        private CRaum FindDups(CRaum Raum)
        {
            CRaum Dups = new CRaum();
            foreach (CSwitch LSwitch in Raum.Switches)
                foreach (CPorts LPorts in LSwitch.Ports)
                    if (LPorts.Dosen.Count > 1)
                        foreach (CDose LDose in LPorts.Dosen)
                            Dups.Add(LSwitch.Name, LPorts.Name, LDose.Rack, LDose.Dose, LDose.Bemerkung);
            return Dups;
        }

        private void PrintDups(CRaum Raum, ref string LLog)
        {
            foreach (CSwitch LSwitch in Raum.Switches)
                foreach (CPorts LPorts in LSwitch.Ports)
                    foreach (CDose LDose in LPorts.Dosen)
                        LLog += "Duplikat: " + LSwitch.Name + "," + LPorts.Name + "," + LDose.Rack + "," + LDose.Dose + "," + LDose.Bemerkung + "\r\n";
        }

        private void PrintInfo(CRaum Alle, CRaum Dups, ref string LLog)
        {
            LLog += "Datei-Informationen:\r\n";
            LLog += "Name: " + Regex.Replace(MyWB.FullName, @"\s+", " ") + "\r\n";
            LLog += "Erkannte Switche: " + Alle.NumSwitche + "\r\n";
            LLog += "Belegte Ports insgesamt: " + Alle.NumPorts + "\r\n";
            LLog += "Mehrfach vorkommende Ports: " + Dups.NumPorts + "\r\n";
        }

        public void CheckPorts()
        {
            string LLog = "";
            CRaum Raum = new CRaum();
            FillList(Raum);
            CRaum Dups = FindDups(Raum);
            PrintInfo(Raum, Dups, ref LLog);
            if(Dups.NumSwitche > 0)
                LLog += "\r\nDuplikate:\r\n";
            PrintDups(Dups, ref LLog);
            Form1 MyForm = new Form1(LLog);
            MyForm.Show();
        }

        public int QuietCheck()
        {
            CRaum Raum = new CRaum();
            FillList(Raum);
            CRaum Dups = FindDups(Raum);
            return Dups.NumSwitche;
        }
    }
}
