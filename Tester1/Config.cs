﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Win32;
using System.Diagnostics;

namespace Tester1
{
    class Config
    {
        public static int OptOnSave, StdOnSave = 0;
        public static int OptColSwitch, StdColSwitch = 4;
        public static int OptRowFirst, StdRowFirst = 6;
        public static int OptColPort, StdColPort = 5;
        public static int OptMinSwCount, StdMinSwCount = 5;
        public static int OptColDose, StdColDose = 1;
        public static int OptColBem, StdColBem = 6;
        public static string REG_KEY = "PortChecker";

        static Config()
        {
            RegistryKey MyKey = Registry.CurrentUser.OpenSubKey("Software\\" + REG_KEY);
            if (MyKey != null)
            {
                OptOnSave = Convert.ToInt32(MyKey.GetValue("OnSave") ?? StdOnSave);
                OptColSwitch = Convert.ToInt32(MyKey.GetValue("ColSwitch") ?? StdColSwitch);
                OptRowFirst = Convert.ToInt32(MyKey.GetValue("RowFirst") ?? StdRowFirst);
                OptColPort = Convert.ToInt32(MyKey.GetValue("ColPort") ?? StdColPort);
                OptMinSwCount = Convert.ToInt32(MyKey.GetValue("MinSwCount") ?? StdMinSwCount);
                OptColDose = Convert.ToInt32(MyKey.GetValue("ColDose") ?? StdColDose);
                OptColBem = Convert.ToInt32(MyKey.GetValue("ColBem") ?? StdColBem);
            }
        }
    }
}
