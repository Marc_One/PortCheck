﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;

using System.Windows.Forms;

namespace Tester1
{
    public partial class ThisAddIn
    {
        void MyHandler(Excel.Workbook Wb, bool SaveAsUI, ref bool Cancel)
        {
            CCheckPorts OCheckPorts = new CCheckPorts();
            if (Config.OptOnSave != 0 && OCheckPorts.QuietCheck() > 0 && Convert.ToString(Wb.Sheets[1].Cells(4,1).Value ?? "") == "Rack/Panel/Port")
            {
                if (DialogResult.Yes == MessageBox.Show("Es wurden mehrfache Ports erkannt. Trotzdem Speichern?", "Duplikat Tester", MessageBoxButtons.YesNo))
                    Cancel = false;
                else
                {
                    Cancel = true;
                    OCheckPorts.CheckPorts();
                }
            }
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.WorkbookBeforeSave += new Excel.AppEvents_WorkbookBeforeSaveEventHandler(MyHandler);
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region Von VSTO generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
