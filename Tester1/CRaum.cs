﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace Tester1
{
    class CDose
    {
        public string Rack;
        public string Dose;
        public string Bemerkung;
    }

    class CPorts
    {
        public string Name;
        public List<CDose> Dosen;

        public void Add(string Rack, string Dose, string Bemerkung)
        {
            bool btest = false;
            foreach (CDose LDose in Dosen)
                if (LDose.Dose == Dose && LDose.Rack == Rack)
                    btest = true;
            if (!btest)
            {
                CDose LDose = new CDose();
                LDose.Rack = Rack;
                LDose.Dose = Dose;
                LDose.Bemerkung = Bemerkung;
                Dosen.Add(LDose);
            }
        }

        public CPorts()
        {
            Dosen = new List<CDose>();
        }
    }

    class CSwitch
    {
        public string Name;
        public List<CPorts> Ports;

        public void Add(string Port, string Rack, string Dose, string Bemerkung)
        {
            bool btest = false;
            foreach (CPorts LPort in Ports)
                if (LPort.Name == Port)
                {
                    LPort.Add(Rack, Dose, Bemerkung);
                    btest = true;
                }
            if (!btest)
            {
                CDose LDose = new CDose();
                LDose.Rack = Rack;
                LDose.Dose = Dose;
                LDose.Bemerkung = Bemerkung;
                CPorts LPorts = new CPorts();
                LPorts.Name = Port;
                LPorts.Dosen.Add(LDose);
                Ports.Add(LPorts);
            }
        }

        public CSwitch()
        {
            Ports = new List<CPorts>();
        }
    }

    class CRaum
    {
        public List<CSwitch> Switches;

        public int NumSwitche
        {
            get
            {
                return Switches.Count;
            }
        }
        public int NumPorts
        {
            get
            {
                int i = 0;
                foreach (CSwitch Switch in Switches)
                    i += Switch.Ports.Count;
                return i;
            }
        }
        public int NumDosen
        {
            get
            {
                int i = 0;
                foreach (CSwitch Switch in Switches)
                    foreach (CPorts Ports in Switch.Ports)
                        i += Ports.Dosen.Count;
                return i;
            }
        }

        public void Add(string Switch, string Port, string Rack, string Dose, string Bemerkung)
        {
            bool btest = false;
            foreach (CSwitch LSwitch in Switches)
                if (LSwitch.Name == Switch)
                {
                    LSwitch.Add(Port, Rack, Dose, Bemerkung);
                    btest = true;
                }
            if (!btest)
            {
                CDose LDose = new CDose();
                LDose.Rack = Rack;
                LDose.Dose = Dose;
                LDose.Bemerkung = Bemerkung;
                CPorts LPorts = new CPorts();
                LPorts.Name = Port;
                LPorts.Dosen.Add(LDose);
                CSwitch LSwitch = new CSwitch();
                LSwitch.Name = Switch;
                LSwitch.Ports.Add(LPorts);
                Switches.Add(LSwitch);
            }
        }

        public CRaum()
        {
            Switches = new List<CSwitch>();
        }
    }
}
