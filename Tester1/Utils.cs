﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace Tester1
{
    class Utils
    {
        public static bool IsSwitch(string Switch)
        {
            Switch = Switch.ToUpper();
            Regex MyReg = new Regex(@"^SW[A-Z]\d-[A-Z0-9]{3}$");
            return MyReg.IsMatch(Switch);
        }

        public static bool IsDose(string Dose)
        {
            Regex LWLReg = new Regex(@"^[A-Z]-\d\d-\d\d/\d\d$");
            Regex TPReg = new Regex(@"^[A-Z]-\d\d-\d\d$");
            return LWLReg.IsMatch(Dose) || TPReg.IsMatch(Dose);
        }

        public static bool IsPort(string Port)
        {
            Regex StackReg = new Regex(@"^\d\d$");
            Regex ModReg = new Regex(@"^[A-Z]\d\d$");
            return StackReg.IsMatch(Port) || ModReg.IsMatch(Port);
        }

        public static bool IsDbe(string DBe)
        {
            Regex MyReg = new Regex(@"^.*-\d{1,2}$");
            return MyReg.IsMatch(DBe) || DBe.ToUpper() == "FREI";
        }

        public static bool IsRaum(string Raum)
        {
            return Raum != "";
        }

        public static bool IsLine(string Dose, string DBe, string Switch, string Port)
        {
            return IsDose(Dose) && IsDbe(DBe) && IsSwitch(Switch) && IsPort(Port);
        }

        public static bool IsListOK(string Dose, string Raum, string DBe)
        {
            return (DBe == "frei" && IsDose(Dose)) || (IsDose(Dose) && IsRaum(Raum) && IsDbe(DBe));
        }

        public static bool IsPatchOK(string Switch, string Port)
        {
            return Switch != "" && Port != "";
        }
    }
}
