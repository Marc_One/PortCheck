﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Printing;

namespace Tester1
{
    public partial class Form1 : Form
    {
        string MyLog;
        public Form1(string LLog)
        {
            InitializeComponent();

            textBox1.Text = LLog;
            textBox1.Select(0, 0);
            MyLog = LLog;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void bPrint_Click(object sender, EventArgs e)
        {
            PrintDocument PrintDoc = new PrintDocument();
            PrintDoc.DocumentName = "Port Duplikat Tester Ausgabe";
            PrintDoc.PrintPage += new PrintPageEventHandler(printDoc_PrintPage);
            PrintDialog PrintDialog1 = new PrintDialog();
            PrintDialog1.Document = PrintDoc;
            PrintDialog1.AllowSomePages = false;

            DialogResult dialogResult = PrintDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                PrintDoc.Print();
            }

        }

        private void printDoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            Font font = new Font("Consolas", 12);

            g.DrawRectangle(Pens.Black, e.MarginBounds); 
            g.DrawString(MyLog, font, Brushes.Black, e.MarginBounds);

            e.HasMorePages = false;
        }
    }
}
